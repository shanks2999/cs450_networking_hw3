"""
war card game client and server
"""
import asyncio
from collections import namedtuple
from enum import Enum
import logging
import random
import socket
from _thread import *
import sys
import time

"""
Namedtuples work like classes, but are much more lightweight so they end
up being faster. It would be a good idea to keep objects in each of these
for each game which contain the game's state, for instance things like the
socket, the cards given, the cards still available, etc.
"""
Game = namedtuple("Game", ["p1", "p2"])


class Command(Enum):
    """
    The byte values sent as the first byte of any message in the war protocol.
    """
    WANTGAME = 0
    GAMESTART = 1
    PLAYCARD = 2
    PLAYRESULT = 3


class Result(Enum):
    """
    The byte values sent as the payload byte of a PLAYRESULT message.
    """
    WIN = 0
    DRAW = 1
    LOSE = 2


def readexactly(sock, numbytes):
    """
    Accumulate exactly `numbytes` from `sock` and return those. If EOF is found
    before numbytes have been received, be sure to account for that here or in
    the caller.
    """
    response = sock.recv(1024)
    data_filter = []
    try:
        for index in range(numbytes):
            data_filter.append(int.from_bytes(response[index:index+1], byteorder='little', signed=True))
    except IndexError:
        print("End Of File .")
        return data_filter
    return data_filter
    pass


def kill_game(game):
    """
    TODO: If either client sends a bad message, immediately nuke the game.
    """
    print("A game session is killed !!!")
    game[0].close()
    game[1].close()
    pass


def compare_cards(card1, card2):
    """
    TODO: Given an integer card representation, return -1 for card1 < card2,
    0 for card1 = card2, and 1 for card1 > card2
    """
    card1 += 1
    card2 += 1
    if card1 % 13 == 0:
        card1 = 13
    else:
        card1 = card1 % 13

    if card2 % 13 == 0:
        card2 = 13
    else:
        card2 = card2 % 13

    if card1 < card2:
        return -1
    if card1 > card2:
        return 1
    if card1 == card2:
        return 0
    pass


def deal_cards():
    """
    TODO: Randomize a deck of cards (list of ints 0..51), and return two
    26 card "hands."
    """
    deck = []
    p1_deck = bytearray()
    p2_deck = bytearray()
    for card in range(52):
        deck.append(card)
    random.shuffle(deck)
    # print(deck[0:26])
    # print(deck[26:52])
    # deck1 = []
    # deck2 = []
    #
    # for card in range(0, 26):
    #     deck1.append(deck[card]+1)
    #     deck2.append(deck[card+26]+1)
    #
    # for card in range(0, 26):
    #     if deck1[card] % 13 == 0:
    #         deck1[card] = 13
    #     else:
    #         deck1[card] = deck1[card] % 13
    #     if deck2[card] % 13 == 0:
    #         deck2[card] = 13
    #     else:
    #         deck2[card] = deck2[card] % 13
    # print(deck1)
    # print(deck2)

    basket_p1 = set()
    basket_p2 = set()
    for card in range(0, 26):
        basket_p1.add(deck[card])
    for card in range(26, 52):
        basket_p2.add(deck[card])

    for card in range(0, 26):
        p1_deck += bytes(deck[card].to_bytes(1, byteorder='little', signed=True))
    for card in range(26, 52):
        p2_deck += bytes(deck[card].to_bytes(1, byteorder='little', signed=True))
    return basket_p1, basket_p2, p1_deck, p2_deck
    pass


def serve_game(host, port):
    """
    TODO: Open a socket for listening for new connections on host:port, and
    perform the war protocol to serve a game of war between each client.
    This function should run forever, continually serving clients.
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((host, port))
    print("Listening for connections...")
    sock.listen(10000)

    def threaded_client(game_tuple):
        p1_rec = readexactly(game_tuple.p1, 1)
        p2_rec = readexactly(game_tuple.p2, 1)
        if len(p1_rec) != 1 or len(p1_rec) != 1 or \
                p1_rec[0] != Command.WANTGAME.value or p2_rec[0] != Command.WANTGAME.value:
            kill_game(game_tuple)
            return
        basket_p1, basket_p2, p1_deck, p2_deck = deal_cards()
        p1_deck = bytes(Command.GAMESTART.value.to_bytes(1, byteorder='little', signed=True)) + p1_deck
        p2_deck = bytes(Command.GAMESTART.value.to_bytes(1, byteorder='little', signed=True)) + p2_deck
        game_tuple.p1.send(p1_deck)
        game_tuple.p2.send(p2_deck)

        for index in range(26):
            p1_rec = readexactly(game_tuple.p1, 2)
            p2_rec = readexactly(game_tuple.p2, 2)
            if len(p1_rec) != 2 or len(p2_rec) != 2:
                kill_game(game_tuple)
                return
            if p1_rec[0] != p2_rec[0] or p1_rec[0] != Command.PLAYCARD.value:
                kill_game(game_tuple)
                return
            if p1_rec[1] in basket_p2 or p2_rec[1] in basket_p1:
                kill_game(game_tuple)
                return
            if p1_rec[1] not in basket_p1 or p2_rec[1] not in basket_p2:
                kill_game(game_tuple)
                return
            p1_res = compare_cards(p1_rec[1], p2_rec[1])
            p2_res = compare_cards(p2_rec[1], p1_rec[1])
            p1_res = bytes(Command.PLAYRESULT.value.to_bytes(1, byteorder='little', signed=True))\
                + bytes(p1_res.to_bytes(1, byteorder='little', signed=True))
            p2_res = bytes(Command.PLAYRESULT.value.to_bytes(1, byteorder='little', signed=True))\
                + bytes(p2_res.to_bytes(1, byteorder='little', signed=True))
            game_tuple.p1.send(p1_res)
            game_tuple.p2.send(p2_res)
            basket_p1.remove(p1_rec[1])
            basket_p2.remove(p2_rec[1])
        print("A game is finished")
        game_tuple.p1.close()
        game_tuple.p2.close()
        return

    clients = []
    count = 0
    try:
        while True:
            conn = sock.accept()[0]
            clients.append(conn)
            count += 1
            if len(clients) == 2:
                print("2 clients joined")
                game_tuple_temp = Game(clients[0], clients[1])
                start_new_thread(threaded_client, (game_tuple_temp,))
                clients.clear()
                if count % 100 == 0:
                    time.sleep(1)
    except(KeyboardInterrupt, SystemExit):
        print("Keyboard Interrupt detected !")
        sys.exit()
    pass


async def limit_client(host, port, loop, sem):
    """
    Limit the number of clients currently executing.
    You do not need to change this function.
    """
    async with sem:
        return await client(host, port, loop)


async def client(host, port, loop):
    """
    Run an individual client on a given event loop.
    You do not need to change this function.
    """
    try:
        reader, writer = await asyncio.open_connection(host, port, loop=loop)
        # send want game
        writer.write(b"\0\0")
        card_msg = await reader.readexactly(27)
        myscore = 0
        for card in card_msg[1:]:
            writer.write(bytes([Command.PLAYCARD.value, card]))
            result = await reader.readexactly(2)
            if result[1] == Result.WIN.value:
                myscore += 1
            elif result[1] == Result.LOSE.value:
                myscore -= 1
        if myscore > 0:
            result = "won"
        elif myscore < 0:
            result = "lost"
        else:
            result = "drew"
        logging.debug("Game complete, I %s", result)
        writer.close()
        return 1
    except ConnectionResetError:
        logging.error("ConnectionResetError")
        return 0
    except asyncio.streams.IncompleteReadError:
        logging.error("asyncio.streams.IncompleteReadError")
        return 0
    except OSError:
        logging.error("OSError")
        return 0


def main(args):
    """
    launch a client/server
    """
    host = args[1]
    port = int(args[2])
    if args[0] == "server":
        try:
            # your server should serve clients until the user presses ctrl+c
            serve_game(host, port)
        except KeyboardInterrupt:
            pass
        return
    else:
        loop = asyncio.get_event_loop()

    if args[0] == "client":
        loop.run_until_complete(client(host, port, loop))
    elif args[0] == "clients":
        sem = asyncio.Semaphore(1000)
        num_clients = int(args[3])
        clients = []
        for x in range(num_clients):
            clients.append(limit_client(host, port, loop, sem))
        # clients = [limit_client(host, port, loop, sem)
        #            for x in range(num_clients)]

        async def run_all_clients():
            """
            use `as_completed` to spawn all clients simultaneously
            and collect their results in arbitrary order.
            """
            completed_clients = 0
            for client_result in asyncio.as_completed(clients):
                completed_clients += await client_result
            return completed_clients
        res = loop.run_until_complete(
            asyncio.Task(run_all_clients(), loop=loop))
        logging.info("%d completed clients", res)

    loop.close()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main(sys.argv[1:])
